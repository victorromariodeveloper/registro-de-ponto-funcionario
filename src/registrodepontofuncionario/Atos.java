package registrodepontofuncionario;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

public class Atos {
    // ArrayList<Nome da Classe do Objeto> nomeDoArrayList = new ArrayList<Nome da Classe do Objeto>();
    ArrayList<Funcionario> funcionarios = new ArrayList<Funcionario>();
    ArrayList<Pontos> pontos = new ArrayList<Pontos>();
    
    Scanner leitura = new Scanner(System.in);
    
    public void telaInicial() {
        String opcao;
        
        System.out.println("a) Cadastrar um funcionário\nb) Registrar ponto\nc) Consultar ponto\nOlá, digite a letra correspondente ao que deseja fazer: ");
        opcao = leitura.next();
        
        if(opcao.equalsIgnoreCase("a")) {
            cadastroFuncionario();
        } else if(opcao.equalsIgnoreCase("b")) {
            registrarPonto();
        } else if(opcao.equalsIgnoreCase("c")) {
            consultaPonto();
        }
    }
    
    public void cadastroFuncionario() {
        String nome, cargo, matricula;
        
        System.out.println("Informe o nome: ");
        nome = leitura.next();
        System.out.println("Informe o cargo: ");
        cargo = leitura.next();
        System.out.println("Informe a matrícula: ");
        matricula = leitura.next();
        
        funcionarios.add(new Funcionario(nome, cargo, matricula));
        
        System.out.println("");
        telaInicial();
    }
    
    public void registrarPonto() {
        String matricula;
        String dataFormatada;
        String horaFormatada;
        int posicaoFuncionario = -1;
        
        System.out.println("Informe a matrícula: ");
        matricula = leitura.next();
        for(int i=0; i<funcionarios.size(); i++) {
            if(matricula.equalsIgnoreCase(funcionarios.get(i).getMatricula())) {
                posicaoFuncionario = i;
            }
        }
        
        if(posicaoFuncionario >= 0) {
            
            Date data = new Date();
            SimpleDateFormat formatador = new SimpleDateFormat("dd/MM/yyyy");
            SimpleDateFormat formatadorHora = new SimpleDateFormat("HH:mm:ss");
            
            dataFormatada = formatador.format(data);
            horaFormatada = formatadorHora.format(data);
            
            if(pontos.size() > 0) {
                for(int i=0; i<pontos.size(); i++) {
                    if(matricula.equalsIgnoreCase(pontos.get(i).getMatricula()) && pontos.get(i).getData().equals(dataFormatada)) {
                        //já existe cadastro
                        if(pontos.get(i).getHoraEntrada().equals("00:00:00")) {
                            pontos.get(i).setHoraEntrada(horaFormatada);
                        } else if(pontos.get(i).getHoraSaida().equals("00:00:00")) {
                            pontos.get(i).setHoraSaida(horaFormatada);
                        } else {
                            System.out.println("Você já registrou o máximo permitido");
                        }
                    } else {
                        //cadastrar primeira vez do dia com lista cheia
                        Pontos pontoFuncionario = new Pontos(
                        funcionarios.get(posicaoFuncionario).getNome(), 
                        funcionarios.get(posicaoFuncionario).getCargo(), 
                        funcionarios.get(posicaoFuncionario).getMatricula(), 
                        dataFormatada);
                        pontoFuncionario.setHoraEntrada(horaFormatada);

                        pontos.add(pontoFuncionario);
                    }
                } 
            } else {
                //cadastrar primeira vez com lista vazia
                Pontos pontoFuncionario = new Pontos(
                        funcionarios.get(posicaoFuncionario).getNome(), 
                        funcionarios.get(posicaoFuncionario).getCargo(), 
                        funcionarios.get(posicaoFuncionario).getMatricula(), 
                        dataFormatada);
                pontoFuncionario.setHoraEntrada(horaFormatada);
                
                pontos.add(pontoFuncionario);
            }
            
            System.out.println("Ponto de "+funcionarios.get(posicaoFuncionario).getNome()+" registrado no dia "+dataFormatada+" as "+horaFormatada);
        } else {
            System.out.println("");
            System.out.println("Nunhum funcionário foi encontrado para esta matrícula.");
        }
        
        System.out.println("");
        telaInicial();
    }
    
    public void consultaPonto() {
        String matricula;
        
        System.out.println("Informe a matrícula: ");
        matricula = leitura.next();
        
        if(pontos.size() > 0) {
            for(int i=0; i<pontos.size(); i++) {
                if(matricula.equals(pontos.get(i).getMatricula())) {
                    System.out.println("");
                    System.out.println("...................................");
                    System.out.println(pontos.get(i).getData()+"\nNome: "+pontos.get(i).getNome()+"\nEntrada: "+pontos.get(i).getHoraEntrada()+"\nSaída: "+pontos.get(i).getHoraSaida());
                    System.out.println("...................................");
                    System.out.println("");
                } else {
                    System.out.println("");
                    System.out.println("...................................");
                    System.out.println("Não existe registro de ponto para essa matrícula!");
                    System.out.println("...................................");
                }
            } 
        } else {
            System.out.println("");
            System.out.println("...................................");
            System.out.println("Não existe ponto a ser consultado!");
            System.out.println("...................................");
        }
        
        System.out.println("");
        telaInicial();
    }
}

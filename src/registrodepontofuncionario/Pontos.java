package registrodepontofuncionario;

public class Pontos {
    private String nome;
    private String cargo;
    private String matricula;
    private String data;
    private String horaEntrada;
    private String horaSaida;
    
    public Pontos(String nome, String cargo, String matricula, String data) {
        this.nome = nome;
        this.cargo = cargo;
        this.matricula = matricula;
        this.data = data;
        this.horaEntrada = "00:00:00";
        this.horaSaida = "00:00:00";
    }
    
    public String getHoraEntrada() {
        return horaEntrada;
    }

    public void setHoraEntrada(String horaEntrada) {
        this.horaEntrada = horaEntrada;
    }

    public String getHoraSaida() {
        return horaSaida;
    }

    public void setHoraSaida(String horaSaida) {
        this.horaSaida = horaSaida;
    }
    
    public String getMatricula() {
        return matricula;
    }

    public String getData() {
        return data;
    }

    public String getNome() {
        return nome;
    }
}

package registrodepontofuncionario;


public class Funcionario {
    private String nome;
    private String cargo;
    private String matricula;
    
    public Funcionario(String nome, String cargo, String matricula) {
        this.nome = nome;
        this.cargo = cargo;
        this.matricula = matricula;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }
    
    
}
